package config

import (
	"sync"

	"github.com/sirupsen/logrus"
)

const (
	ServiceName = "SIMPLE_SERVICE_API"
)

// Config is configuration interface
type Config interface {
	HTTP() *HTTP
	Log() *logrus.Entry
	Authentication() *Authentication
	DB() db.QInterface
}

// ConfigImpl is implementation of config interface
type ConfigImpl struct {
	sync.Mutex

	http *HTTP
	log  *logrus.Entry
	jwt  *Authentication
	db   db.QInterface
}

// New config creating
func New() Config {
	return &ConfigImpl{
		Mutex: sync.Mutex{},
	}
}
