package config

import (
	"github.com/caarlos0/env"
)

// Authentication is store all info about authentication
type Authentication struct {
	VerifyKey string `env:"SIMPLE_SHOP_API_AUTHENTICATION_SECRET,required"`
	Algorithm string `env:"SIMPLE_SHOP_AUTHENTICATION_ALGORITHM" envDefault:"HS256"`
}

// Authentication returns Authentication config
func (c *ConfigImpl) Authentication() *Authentication {
	if c.jwt != nil {
		return c.jwt
	}

	c.Lock()
	defer c.Unlock()

	jwt := &Authentication{}
	if err := env.Parse(jwt); err != nil {
		panic(err)
	}

	c.jwt = jwt

	return c.jwt
}
