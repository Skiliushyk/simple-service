package main

import (
	"log"
	"net/http"
	"os"
	"simple-service/server"
)

const massage = "Hello world"

var (
	SimpleServerAddr = os.Getenv("SIMPLE_SERVER_ADDR")
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", HomeHandler)

	srv := server.New(mux, SimpleServerAddr)

	err := srv.ListenAndServe()
	if err != nil {
		log.Fatalf("server failed to start: %v", err)
	}
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(massage))
}